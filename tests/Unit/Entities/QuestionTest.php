<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 15:49
 */

namespace Unit\Entities;


use App\Contracts\Arrayable;
use App\Contracts\Entity;
use App\Entities\Question;
use PHPUnit\Framework\TestCase;

class QuestionTest extends TestCase
{
    public function testCreate(): void
    {
        $class = new Question('test', 1, 1);

        $this->assertInstanceOf(
            Question::class,
            $class
        );
        $this->assertInstanceOf(
            Entity::class,
            $class
        );
    }

    public function testToArray(): void
    {
        $class = new Question('test', 1, 2);

        $expects = [
            'id' => '',
            'text' => 'test',
            'order' => 1,
            'type' => 2,
            'type_name' => 'Select',
            'is_upload_image' => false,
        ];
        $this->assertEquals($expects, $class->toArray());
        $this->assertInstanceOf(
            Arrayable::class,
            $class
        );
    }

    public function testGetTypeName()
    {
        $class = new Question('test', 1, 3);

        $this->assertEquals('Slider', $class->getTypeName());
    }

    public function testGetType(): void
    {
        $class = new Question('test', 1, 3);

        $this->assertEquals(3, $class->getType());
    }

    public function testIsUploadImageFalse(): void
    {
        $class = new Question('test', 1, 3);

        $this->assertFalse($class->isUploadImage());
    }

    public function testIsUploadImageTrue(): void
    {
        $class = new Question('test', 1, 1);

        $this->assertTrue($class->isUploadImage());
    }
}