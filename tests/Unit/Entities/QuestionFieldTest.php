<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 16:00
 */

namespace Unit\Entities;

use App\Contracts\Arrayable;
use App\Contracts\Entity;
use App\Entities\Question;
use App\Entities\QuestionField;
use PHPUnit\Framework\TestCase;

class QuestionFieldTest extends TestCase
{
    public function testCreate(): void
    {
        $question = new Question('test', 1, 1);;
        $class = new QuestionField($question, 'test', 1, 2);

        $this->assertInstanceOf(
            QuestionField::class,
            $class
        );
        $this->assertInstanceOf(
            Entity::class,
            $class
        );
    }

    public function testToArray(): void
    {
        $question = $this->createMock(Question::class);
        $question->method('getId')
            ->willReturn(1);
        $class = new QuestionField($question, 'test', 1, 2);

        $expects = [
            'id' => null,
            'question_id' => 1,
            'text' => 'test',
            'order' => 2,
            'value' => 1,
            'image' => null
        ];
        $this->assertEquals($expects, $class->toArray());
        $this->assertInstanceOf(
            Arrayable::class,
            $class
        );
    }
}