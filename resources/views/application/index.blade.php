@extends('application.layout')

@section('content')
    @forelse($questions as $question)
        <h2>{{$question['text']}}</h2>
        @if($question['type'] == 1)
            (Image)
        @elseif($question['type'] == 2)
            (Select)
        @elseif($question['type'] == 3)
            (Slider)
        @endif
        <hr>
        @forelse($fields[$question['id']] as $field)
            @if($question['type'] == 1)
                {!! Form::radio($field['id'], $field['value']) !!}
                <img src="/images/{{$field['image']}}">
                {{$field['text']}}
            @elseif($question['type'] == 2)
                {!! Form::radio($field['id'], $field['value']) !!} {{$field['text']}}<br>
            @elseif($question['type'] == 3)
                {!! Form::radio($field['id'], $field['value']) !!} {{$field['text']}}
            @endif
        @empty
            No fields
        @endforelse
    @empty
        No questions
    @endforelse
@endsection