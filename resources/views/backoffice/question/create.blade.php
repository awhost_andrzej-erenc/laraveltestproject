@extends('backoffice.layout')

@section('content')
    {!! Form::open(['url' => $formUrl, 'class' => 'form-horizontal', 'method' => $method]) !!}
    <div class="form-group">
        {!! Form::label('text', 'Question', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('text', isset($question['text']) ? $question['text'] : null, ['class' => 'form-control', 'placeholder' => 'question', 'required', 'autofocus', 'max' => 255]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('order', 'Order', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::number('order', isset($question['order']) ? $question['order'] : null, ['class' => 'form-control', 'placeholder' => 'order', 'required', 'min' => 0]) !!}
        </div>
    </div>
    {!! Form::label('type', 'Type', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('type', [1 => 'Image', 2 => 'Select', 3 => 'Slider'], isset($question['type']) ? $question['type'] : null, ['class' => 'form-control', 'required', 'placeholder' => 'Pick a type']) !!}
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {!! Form::submit($buttonName, ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    {!! Form::close() !!}
@endsection