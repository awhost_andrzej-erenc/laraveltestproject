@extends('backoffice.layout')

@section('content')
    <a href="{!! route('question.create') !!}">
        <button class="btn btn-default">Create</button>
    </a>
    <table class="table">
        <thead>
        <tr>
            <td>Id</td>
            <td>Text</td>
            <td>Order</td>
            <td>Type</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @forelse($questions as $question)
            <tr>
                <td>
                    {{$question['id']}}
                </td>
                <td>
                    {{$question['text']}}
                </td>
                <td>
                    {{$question['order']}}
                </td>
                <td>
                    {{$question['type_name']}}
                </td>
                <td>
                    <a href="{{route('question.edit', ['id' => $question['id']])}}">
                        <button class="btn btn-info">Edit</button>
                    </a>
                    <a href="{{route('field.index', ['id' => $question['id']])}}">
                        <button class="btn btn-info">Fields</button>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td>No questions</td>
            </tr>
        @endforelse
        </tbody>

    </table>
@endsection