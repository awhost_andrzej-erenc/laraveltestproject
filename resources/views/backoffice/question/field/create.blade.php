@extends('backoffice.layout')

@section('content')
    {!! Form::open(['url' => $formUrl, 'class' => 'form-horizontal', 'method' => $method, 'files' => true]) !!}
    <div class="form-group">
        {!! Form::label('text', 'Answer', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::text('text', isset($field['text']) ? $field['text'] : null, ['class' => 'form-control', 'placeholder' => 'answer', 'required', 'autofocus', 'max' => 255]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('value', 'Value', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::number('value', isset($field['value']) ? $field['value'] : null, ['class' => 'form-control', 'placeholder' => 'value', 'required', 'min' => 0]) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('order', 'Order', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10">
            {!! Form::number('order', isset($field['order']) ? $field['order'] : null, ['class' => 'form-control', 'placeholder' => 'order', 'required', 'min' => 0]) !!}
        </div>
    </div>
    @if($question['is_upload_image'] == true)
        @if(isset($field) && $field['image'] != null)
            <img src="/images/{{$field['image']}}">
        @endif
        <div class="form-group">
            {!! Form::label('image', 'Image', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::file('image', null, ['class' => 'form-control', 'placeholder' => 'image', 'required']) !!}
            </div>
        </div>
    @endif
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {!! Form::submit($buttonName, ['class' => 'btn btn-success']) !!}
        </div>
    </div>
    {!! Form::close() !!}
@endsection