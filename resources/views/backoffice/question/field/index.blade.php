@extends('backoffice.layout')

@section('content')
    <a href="{!! route('field.create', ['id' => $id]) !!}">
        <button class="btn btn-default">Create</button>
    </a>
    <table class="table">
        <thead>
        <tr>
            <td>Id</td>
            <td>Text</td>
            <td>Value</td>
            <td>Order</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @forelse($fields as $field)
            <tr>
                <td>
                    {{$field['id']}}
                </td>
                <td>
                    {{$field['text']}}
                </td>
                <td>
                    {{$field['value']}}
                </td>
                <td>
                    {{$field['order']}}
                </td>
                <td>
                    <a href="{{route('field.edit', ['question_id' => $id, 'field' => $field['id']])}}">
                        <button class="btn btn-info">Edit</button>
                    </a>
                </td>
            </tr>
        @empty
            <tr>
                <td>No fields</td>
            </tr>
        @endforelse
        </tbody>

    </table>
@endsection