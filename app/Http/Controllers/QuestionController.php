<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 12:03
 */

namespace App\Http\Controllers;


use App\Entities\Question;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Factory as Validator;

class QuestionController extends Controller
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var Validator
     */
    private $validate;

    public function __construct(\Doctrine\ORM\EntityManager $em, Validator $validator)
    {
        $this->em = $em;
        $this->validate = $validator;
    }

    public function index(): Response
    {
        $questions = $this->em->createQueryBuilder()
            ->select('q')
            ->from('App\Entities\Question', 'q')
            ->orderBy('q.order')
            ->getQuery()
            ->getResult();

        $response = [];

        /**
         * @var Question $question
         */
        foreach ($questions as $question) {
            $response[] = $question->toArray();
        }

        return response($response, 200);
    }

    public function store(Request $request): Response
    {
        $validate = $this->validate->make($request->toArray(), [
            'text' => 'required|max:255',
            'type' => 'required',
            'order' => 'required',
        ]);
        //TODO require fields to choose

        if ($validate->fails()) {
            return response($validate->errors(), 400);
        }

        $question = new Question($request->get('text'), $request->get('order'), $request->get('type'));
        $this->em->persist($question);
        $this->em->flush();

        return response('', 201);
    }

    public function show(int $id): Response
    {
        /**
         * @var Question $question
         */
        $question = $this->em->createQueryBuilder()
            ->select('q')
            ->from('App\Entities\Question', 'q')
            ->where('q.id = ?1')
            ->setParameter(1, $id)
            ->getQuery()
            ->getOneOrNullResult();

        if ($question == null) {
            return response('', 404);
        }

        return response($question->toArray(), 200);
    }

    public function update(Request $request, int $id): Response
    {
        /**
         * @var Question $question
         */
        $question = $this->em->createQueryBuilder()
            ->select('q')
            ->from('App\Entities\Question', 'q')
            ->where('q.id = ?1')
            ->setParameter(1, $id)
            ->getQuery()
            ->getOneOrNullResult();

        if ($question == null) {
            return response('', 404);
        }

        $validate = $this->validate->make($request->toArray(), [
            'text' => 'required|max:255',
            'type' => 'required',
            'order' => 'required',
        ]);
        //TODO require fields to choose

        if ($validate->fails()) {
            return response($validate->errors(), 400);
        }

        $question->setText($request->get('text'));
        $question->setType($request->get('type'));
        $question->setOrder($request->get('order'));

        $this->em->persist($question);
        $this->em->flush();

        return response('', 200);
    }
}