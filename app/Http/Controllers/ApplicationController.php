<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 21:05
 */

namespace App\Http\Controllers;


use GuzzleHttp\Client;
use Illuminate\View\View;

class ApplicationController extends Controller
{
    public function index(): View
    {
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions');
        $questions = json_decode($res->getBody(), true);

        $fields = [];

        foreach ($questions as $question) {
            $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions/' . $question['id'] . '/fields');
            $fields[$question['id']] = json_decode($res->getBody(), true);
        }

        return view('application.index', compact('questions', 'fields'));
    }
}