<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 13:23
 */

namespace App\Http\Controllers\Backoffice;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\View\View;

class QuestionController extends BackofficeController
{
    public function index(): View
    {
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions/');
        $questions = json_decode($res->getBody(), true);

        return view('backoffice.question.index', compact('questions'));
    }

    public function create(): View
    {
        $formUrl = route('question.store');
        $buttonName = 'Create';
        $method = 'POST';
        return view('backoffice.question.create', compact('formUrl', 'buttonName', 'method'));
    }

    public function store(Request $request)
    {
        $client = new Client();
        try {
            $res = $client->request('POST', 'http://127.0.0.1:8888/api/questions', [
                'form_params' => [
                    'text' => $request->get('text'),
                    'order' => $request->get('order'),
                    'type' => $request->get('type'),
                ]
            ]);
        } catch (RequestException $e) {
            return \Redirect::route('question.create')->withErrors(['Not all field has correct data']);
        }

        return \Redirect::route('question.index');
    }

    public function edit(int $id): View
    {
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions/' . $id);
        $question = json_decode($res->getBody(), true);

        $formUrl = route('question.update', ['id' => $id]);
        $buttonName = 'Update';
        $method = 'PUT';
        return view('backoffice.question.create', compact('formUrl', 'question', 'buttonName', 'method'));
    }

    public function update(Request $request, int $id)
    {
        $client = new Client();
        try {
            $res = $client->request('PUT', 'http://127.0.0.1:8888/api/questions/' . $id, [
                'form_params' => [
                    'text' => $request->get('text'),
                    'order' => $request->get('order'),
                    'type' => $request->get('type'),
                ]
            ]);
        } catch (RequestException $e) {
            return \Redirect::route('question.edit', ['id' => $id])->withErrors(['Not all field has correct data']);
        }

        return \Redirect::route('question.index');
    }
}