<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 18:49
 */

namespace App\Http\Controllers\Backoffice;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\View\View;

class QuestionFieldController extends BackofficeController
{
    public function index(int $id): View
    {
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions/' . $id . '/fields');
        $fields = json_decode($res->getBody(), true);

        return view('backoffice.question.field.index', compact('fields', 'id'));
    }

    public function create(int $id): View
    {
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions/' . $id);
        $question = json_decode($res->getBody(), true);

        $formUrl = route('field.store', ['id' => $id]);
        $buttonName = 'Create';
        $method = 'POST';
        return view('backoffice.question.field.create', compact('formUrl', 'question', 'buttonName', 'method'));
    }

    public function store(Request $request, int $id)
    {
        $data = [
            'text' => $request->get('text'),
            'value' => $request->get('value'),
            'order' => $request->get('order')
        ];

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $imageFileName = $id . '_' . md5(time()) . '.' . $request->image->extension();
            $request->image->storeAs('images', $imageFileName);
            $data['image'] = $imageFileName;
        }

        $client = new Client();
        try {
            $res = $client->request('POST', 'http://127.0.0.1:8888/api/questions/' . $id . '/fields', [
                'form_params' => $data
            ]);
        } catch (RequestException $e) {
            return \Redirect::route('field.create', ['id' => $id])->withErrors(['Not all field has correct data']);
        }

        return \Redirect::route('field.index', ['id' => $id]);
    }

    public function edit(int $questionId, int $fieldId): View
    {
        $client = new Client();
        $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions/' . $questionId . '/fields/' . $fieldId);
        $field = json_decode($res->getBody(), true);

        $res = $client->request('GET', 'http://127.0.0.1:8888/api/questions/' . $questionId);
        $question = json_decode($res->getBody(), true);

        $formUrl = route('field.update', ['question_id' => $questionId, 'field' => $fieldId]);
        $buttonName = 'Update';
        $method = 'PUT';
        return view('backoffice.question.field.create', compact('formUrl', 'question', 'field', 'buttonName', 'method'));
    }

    public function update(Request $request, int $questionId, int $fieldId)
    {
        $data = [
            'text' => $request->get('text'),
            'value' => $request->get('value'),
            'order' => $request->get('order')
        ];

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $imageFileName = $questionId . '_' . md5(time()) . '.' . $request->image->extension();
            $request->image->storeAs('images', $imageFileName);
            $data['image'] = $imageFileName;
        }

        $client = new Client();
        try {
            $res = $client->request('PUT', 'http://127.0.0.1:8888/api/questions/' . $questionId . '/fields/' . $fieldId, [
                'form_params' => $data
            ]);
        } catch (RequestException $e) {
            return \Redirect::route('field.edit', ['question_id' => $questionId, 'field' => $fieldId])->withErrors(['Not all field has correct data']);
        }

        return \Redirect::route('field.index', ['id' => $questionId]);
    }
}