<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 12:53
 */

namespace App\Http\Controllers;

use App\Entities\Question;
use App\Entities\QuestionField;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Factory as Validator;

class QuestionFieldController
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var Validator
     */
    private $validate;

    /**
     * @var Storage
     */
    private $storage;

    public function __construct(\Doctrine\ORM\EntityManager $em, Validator $validator)
    {
        $this->em = $em;
        $this->validate = $validator;
    }

    public function index(int $id): Response
    {
        $questionsFields = $this->em->createQueryBuilder()
            ->select('q')
            ->from('App\Entities\QuestionField', 'q')
            ->where('q.question = ?1')
            ->setParameter(1, $id)
            ->orderBy('q.order')
            ->getQuery()
            ->getResult();

        $response = [];

        /**
         * @var QuestionField $questionField
         */
        foreach ($questionsFields as $questionField) {
            $response[] = $questionField->toArray();
        }

        return response($response, 200);
    }

    public function store(Request $request, int $questionId): Response
    {
        $validate = $this->validate->make($request->toArray(), [
            'text' => 'required|max:255',
            'value' => 'required',
            'order' => 'required',
            'image' => 'string',
        ]);

        if ($validate->fails()) {
            return response($validate->errors(), 400);
        }

        /**
         * @var Question $question
         */
        $question = $this->em->createQueryBuilder()
            ->select('q')
            ->from('App\Entities\Question', 'q')
            ->where('q.id = ?1')
            ->setParameter(1, $questionId)
            ->getQuery()
            ->getOneOrNullResult();

        if ($question == null) {
            return response('Question id not exist.', 400);
        }

        $image = null;
        if ($question->isUploadImage() && $request->has('image')) {
            $image = $request->get('image');
        }

        $questionField = new QuestionField($question, $request->get('text'), $request->get('value'), $request->get('order'), $image);
        $this->em->persist($questionField);
        $this->em->flush();

        return response('', 201);
    }

    public function show(int $questionId, int $fieldId): Response
    {
        /**
         * @var QuestionField $field
         */
        $field = $this->em->createQueryBuilder()
            ->select('q')
            ->from('App\Entities\QuestionField', 'q')
            ->where('q.id = ?1')
            ->setParameter(1, $fieldId)
            ->andWhere('q.question = ?2')
            ->setParameter(2, $questionId)
            ->getQuery()
            ->getOneOrNullResult();

        if ($field == null) {
            return response('', 404);
        }

        return response($field->toArray(), 200);
    }

    public function update(Request $request, int $questionId, int $fieldId): Response
    {
        /**
         * @var QuestionField $field
         */
        $field = $this->em->createQueryBuilder()
            ->select('q')
            ->from('App\Entities\QuestionField', 'q')
            ->where('q.id = ?1')
            ->setParameter(1, $fieldId)
            ->andWhere('q.question = ?2')
            ->setParameter(2, $questionId)
            ->getQuery()
            ->getOneOrNullResult();

        if ($field == null) {
            return response('', 404);
        }

        $validate = $this->validate->make($request->toArray(), [
            'text' => 'required|max:255',
            'value' => 'required',
            'order' => 'required',
            'image' => 'string',
        ]);

        if ($validate->fails()) {
            return response($validate->errors(), 400);
        }

        $field->setText($request->get('text'));
        $field->setValue($request->get('value'));
        $field->setOrder($request->get('order'));

        if ($request->has('image')) {
            $field->setImage($request->get('image'));
        }

        $this->em->persist($field);
        $this->em->flush();

        return response('', 200);
    }
}