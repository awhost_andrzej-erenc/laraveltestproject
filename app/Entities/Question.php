<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 11:58
 */

namespace App\Entities;

use App\Contracts\Arrayable;
use App\Contracts\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Table(name="Questions")
 */
class Question implements Entity, Arrayable
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $text;

    /**
     * @var integer
     * @ORM\Column(name="`order`", type="integer", nullable=false)
     */
    private $order;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=false)
     */
    private $type;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Question constructor.
     * @param string $text
     * @param int $order
     * @param int $type
     */
    public function __construct(string $text, int $order, int $type)
    {
        $this->text = $text;
        $this->order = $order;
        $this->type = $type;
    }

    /**
     * @codeCoverageIgnore
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @codeCoverageIgnore
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    public function getTypeName(): string
    {
        $types = [
            1 => 'Image',
            2 => 'Select',
            3 => 'Slider',
        ];

        return $types[$this->type];
    }

    public function isUploadImage(): bool
    {
        if ($this->type == 1) {
            return true;
        }

        return false;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'order' => $this->order,
            'type' => $this->type,
            'type_name' => $this->getTypeName(),
            'is_upload_image' => $this->isUploadImage(),
        ];
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order)
    {
        $this->order = $order;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }
}