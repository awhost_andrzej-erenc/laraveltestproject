<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 12:53
 */

namespace App\Entities;

use App\Contracts\Arrayable;
use App\Contracts\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity
 * @ORM\Table(name="QuestionFields")
 */
class QuestionField implements Entity, Arrayable
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="Question")
     */
    private $question;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $text;

    /**
     * @var integer
     * @ORM\Column(name="`value`", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @var integer
     * @ORM\Column(name="`order`", type="integer", nullable=false)
     */
    private $order;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct(Question $question, $text, $value, $order, $image = null)
    {
        $this->question = $question;
        $this->text = $text;
        $this->value = $value;
        $this->order = $order;
        $this->image = $image;
    }

    /**
     * @codeCoverageIgnore
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @codeCoverageIgnore
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value)
    {
        $this->value = $value;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image)
    {
        $this->image = $image;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order)
    {
        $this->order = $order;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'question_id' => $this->question->getId(),
            'text' => $this->text,
            'value' => $this->value,
            'order' => $this->order,
            'image' => $this->image,
        ];
    }
}