<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 12.07.2017
 * Time: 15:55
 */

namespace App\Contracts;


interface Arrayable
{
    public function toArray(): array;
}